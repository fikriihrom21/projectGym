package com.example.projectgym;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText etUsername,etPassword;
    Button btnLogin;
    String Username, Password;
    TextView tvRegister;
    ProgressBar loadingLog;
    SessionManager sessionManager;
    public static final  String URL_LOGIN = "http://192.168.1.10" + "/android_register_login/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //hide actionBar
        getSupportActionBar().hide();

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);

        btnLogin = findViewById(R.id.btnLogin);
        loadingLog = findViewById(R.id.loadingLogin);

        sessionManager = new SessionManager(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: berhasil");
                Username = etUsername.getText().toString().trim();
                Password = etPassword.getText().toString().trim();

                if(!Username.isEmpty() || !Password.isEmpty()){
                    login(Username,Password);
                }
                else {
                    Toast.makeText(LoginActivity.this, "Username dan password harus di isi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //fuction to register
        tvRegister = findViewById(R.id.tvCreateAccount);
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void login(String username, String password) {

        btnLogin.setVisibility(View.GONE);
        tvRegister.setVisibility(View.GONE);
        loadingLog.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("login");

                            if (success.equals("1")){
                                btnLogin.setVisibility(View.VISIBLE);
                                tvRegister.setVisibility(View.VISIBLE);
                                loadingLog.setVisibility(View.GONE);

                                for(int i = 0;i<jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    Toast.makeText(LoginActivity.this, "Login Success : "+object.getString("name").trim(), Toast.LENGTH_SHORT).show();

                                    String userName = object.getString("name");
                                    String email = object.getString("email");

                                    sessionManager.createSession(userName,email);
                                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            btnLogin.setVisibility(View.VISIBLE);
                            tvRegister.setVisibility(View.VISIBLE);
                            loadingLog.setVisibility(View.GONE);
                            Toast.makeText(LoginActivity.this, "Login Error : "+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                btnLogin.setVisibility(View.VISIBLE);
                tvRegister.setVisibility(View.VISIBLE);
                loadingLog.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Login Error : "+error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                //posting parameters he post url
                Map<String, String> params = new HashMap<>();

                params.put("username", username);
                params.put("password", password);

                return params;

            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(stringRequest);

    }
}