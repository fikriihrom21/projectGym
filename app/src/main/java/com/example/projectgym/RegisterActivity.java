package com.example.projectgym;

import static android.content.ContentValues.TAG;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity{

    EditText etUsername,etPassword,etName,etEmail,etAddress,etPhone,etDate, etCPass;
    Button btnRegister;
    TextView tvLogin;
    Calendar myCalendar;
    int year,month,day;
    RadioGroup list_gender;
    RadioButton radioSexButton;
    int checkedId = 0;

    ProgressBar loadingReg;

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 4 characters
                    "$");
    private static final Pattern PHONE_PATTERN = Pattern.compile("^(\\+62|62|0)8[1-9][0-9]{6,9}$");

    public static final  String URL_REGISTER = "http://192.168.1.10" + "/android_register_login/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //hide actionBar
        getSupportActionBar().hide();

        etUsername = findViewById(R.id.etRegisterUsername);
        etPassword = findViewById(R.id.etRegisterPassword);
        etName = findViewById(R.id.etRegisterName);
        etEmail = findViewById(R.id.etRegisterEmail);
        etAddress = findViewById(R.id.etRegisterAddressUser);
        etPhone = findViewById(R.id.etRegisterPhoneUser);
        etCPass = findViewById(R.id.etKonfirmasiPassword);
        list_gender = findViewById(R.id.radiogroupgender);
        loadingReg = findViewById(R.id.loadingRegist);

        //function date
        etDate = findViewById(R.id.etRegisterDOB);
        myCalendar = Calendar.getInstance();
        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month= month+1;
                        String sdate = year+"-"+month+"-"+dayOfMonth;
                        etDate.setText(sdate);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        //register button
        btnRegister = findViewById(R.id.btnRegister);
      btnRegister.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              checkedId = list_gender.getCheckedRadioButtonId();

              if (etUsername.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "username harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (etName.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "name harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (etEmail.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "email harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (etPhone.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "telepon harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (etAddress.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "Address harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (etDate.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "tanggal lahir harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (etPassword.getText().toString().isEmpty()){
                  Toast.makeText(RegisterActivity.this, "password harus diisi !", Toast.LENGTH_SHORT).show();
              }else if (checkedId < 1){
                  Toast.makeText(RegisterActivity.this, "gender harus dipilih !", Toast.LENGTH_SHORT).show();
              }
              else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()){
                  Toast.makeText(RegisterActivity.this, "email harus sesuai dengan format !", Toast.LENGTH_SHORT).show();
              }else if (!PASSWORD_PATTERN.matcher(etPassword.getText().toString()).matches()){
                  Toast.makeText(RegisterActivity.this, "password harus terdiri dari 1 angka, 1 huruf kapital, dan tidak kurang dari 4 karakter !", Toast.LENGTH_SHORT).show();
              }
              else if (!etCPass.getText().toString().matches(etPassword.getText().toString())){
                  Toast.makeText(RegisterActivity.this, "Konfirmasi password tidak sama dengan password yang dimasukkan !", Toast.LENGTH_SHORT).show();
              }
              else if (!PHONE_PATTERN.matcher(etPhone.getText().toString()).matches()){
                  Toast.makeText(RegisterActivity.this, "nomor telepon seluler harus sesuai format !", Toast.LENGTH_SHORT).show();
              }
              else {
                  radioSexButton = (RadioButton) findViewById(checkedId);
                  register();
              }
          }
      });

        //button to login
        tvLogin = findViewById(R.id.tvLoginHere);
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLogin();
            }
        });
    }

    void register() {

        btnRegister.setVisibility(View.GONE);
        tvLogin.setVisibility(View.GONE);
        loadingReg.setVisibility(View.VISIBLE);

        final String Username = etUsername.getText().toString();
        final String Password = etPassword.getText().toString();
        final String Name = etName.getText().toString();
        final String Email = etEmail.getText().toString();
        final String Address = etAddress.getText().toString();
        final String Phone = etPhone.getText().toString();
        final String Dob = etDate.getText().toString();
        final String Gender = radioSexButton.getText().toString();

        Log.i(TAG, "register: "+ etUsername.getText().toString());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");

                            if (success.equals("1")){
                                goToLogin();
                                btnRegister.setVisibility(View.VISIBLE);
                                tvLogin.setVisibility(View.VISIBLE);
                                loadingReg.setVisibility(View.GONE);
                                Toast.makeText(RegisterActivity.this, "Register Success", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            btnRegister.setVisibility(View.VISIBLE);
                            tvLogin.setVisibility(View.VISIBLE);
                            loadingReg.setVisibility(View.GONE);
                            Toast.makeText(RegisterActivity.this, "Register Error : "+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                btnRegister.setVisibility(View.VISIBLE);
                tvLogin.setVisibility(View.VISIBLE);
                loadingReg.setVisibility(View.GONE);
                Toast.makeText(RegisterActivity.this, "Register Error : "+error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                //posting parameters he post url
                Map<String, String> params = new HashMap<>();

                params.put("username", Username);
                params.put("name", Name);
                params.put("email", Email);
                params.put("alamat", Address);
                params.put("phone", Phone);
                params.put("jenis_kelamin", Gender);
                params.put("tanggal_lahir", Dob);
                params.put("password", Password);

                return params;

            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(stringRequest);
    }

    void goToLogin(){
        Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(intent);
    }
}